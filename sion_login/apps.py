from django.apps import AppConfig


class LoginsionConfig(AppConfig):
    name = 'sion_login'
