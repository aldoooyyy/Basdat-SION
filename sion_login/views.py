from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .db_helper import DatabaseConnection

# Create your views here.
response = {}

def index(request):
    if 'email' in request.session :
        print('logged in')
        response['login'] = True
        return HttpResponseRedirect(reverse('sion-profile:index'))

    else :
        print('not logged in')
        response['login'] = False
        html = 'Login.html'
        return render(request, html, response)

	#def pilihrole(request):
	#html = "PemilihanRole.html"
	#return render(request,html,response)

