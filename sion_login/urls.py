from django.conf.urls import url
from .views import index #pilihrole
from .custom_auth import auth_login, auth_logout
#from .sign_up import registration

urlpatterns = [
    url(r'^$',index,name='index'),
    url(r'^custom_auth/login/$',auth_login,name='auth_login'),
    url(r'^custom_auth/logout/$',auth_logout,name='auth_logout'),
   # url(r'^$',pilihrole,name='pilihrole'),
  #  url(r'^sign_up/registration/$',registration,name='registration'),
   # url(r'^profile/$', profile, name='profile'),
]
