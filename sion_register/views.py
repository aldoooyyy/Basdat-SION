from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from sion_login.db_helper import DatabaseConnection
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

response = {}

def index(request):
	return render(request, 'MilihRole.html', response)

def form_register_organisasi(request):
	return render(request, 'FormPengurusOrganisasi.html', response)

def form_register_relawan(request):
	return render(request, 'FormRegistrasiRelawan.html', response)

def form_register_donatur(request):
	return render(request, 'FormRegistrasiDonatur.html', response)

def form_register_sponsor(request):
	return render(request, 'FormRegistrasiSponsor.html', response)

@csrf_exempt
def register_organisasi(request):
    if 'login' not in request.session or not request.session['login']:
        if(request.method == 'POST'):
            nama_organisasi = request.POST["namaOrganisasiText"]
            website = request.POST["websiteText"]
            email = request.POST["emailText"]
            kecamatan = request.POST["kecamatanText"]
            kabupaten = request.POST["kabupatenText"]
            provinsi = request.POST["provinsiText"]
            kode_pos = request.POST["kodePosText"]
            additional_address = request.POST["additionalAddressText"]

            conn = DatabaseConnection()
            conn.cursor.execute("select * from sion.organisasi where email_organisasi='"+email+"'")
            select = conn.cursor.fetchone()
            if (select):
                messages.error(request, 'Login gagal, email organisasi sudah terdaftar.')
                return HttpResponseRedirect(reverse('sion-login:index'))

            pengurusCounter = 1;
            while ("emailPengurusText"+str(pengurusCounter)) in request.POST:
                conn.cursor.execute("select email from sion.user where email='"+request.POST["emailPengurusText1"]+"';")
                select = conn.cursor.fetchone()
                if (select):
                    messages.error(request, ('Email pengurus '+str(pengurusCounter)+' sudah pernah terdaftar.'))
                    return HttpResponseRedirect(reverse('sion-login:index'))

                pengurusCounter+=1
            
            conn.cursor.execute("insert into organisasi values('"+email+"','"+website+"','"+nama_organisasi+"','"+provinsi+"','"+
                kabupaten+"','"+kecamatan+"','"+additional_address+"','"+str(kode_pos)+"','sudah');")

            conn.cursor.execute("select nomor_registrasi from organisasi_terverifikasi order by nomor_registrasi desc limit 1;")
            select = conn.cursor.fetchone()
            nomor_registrasi = 1
            if (select):
                nomor_registrasi = int(select[0]) + 1
            conn.cursor.execute("insert into organisasi_terverifikasi values('"+email+"','"+str(nomor_registrasi)+"','aktif');")
            messages.info(request, "Nomor Registrasi: " + str(nomor_registrasi))

            tujuanCounter = 1
            while (("tujuanOrganisasiText"+str(tujuanCounter)) in request.POST):
                tujuan = request.POST["tujuanOrganisasiText"+str(tujuanCounter)]
                conn.cursor.execute("insert into tujuan_organisasi values('"+email+"','"+tujuan+"');")
                tujuanCounter+=1

            password = nama_organisasi+'123'
            pengurusCounter -=1
            for i in range(pengurusCounter):
                counter = i+1
                namaPengurus = request.POST["namaPengurusText"+str(counter)]
                emailPengurus = request.POST["emailPengurusText"+str(counter)]
                alamatPengurus = request.POST["namaPengurusText"+str(counter)]

                conn.cursor.execute("insert into sion.user values('"+emailPengurus+"','"+password+"','"+namaPengurus+"','"+alamatPengurus+"');")

                conn.cursor.execute("insert into pengurus_organisasi values('"+emailPengurus+"','"+email+"');")

                messages.info(request, "Email Pengurus "+str(counter)+" terdaftar "+emailPengurus)

            messages.info(request, "Password awal "+password)
           
            return HttpResponseRedirect(reverse('sion-login:index'))            

        return render(request, 'FormPengurusOrganisasi.html', response)