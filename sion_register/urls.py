from django.conf.urls import url
from .views import index, form_register_organisasi, register_organisasi, form_register_relawan, form_register_sponsor, form_register_donatur

urlpatterns = [
    url(r'^$',index,name='index'),
    url(r'^form_register_relawan/$',form_register_relawan,name='form_register_relawan'),
    url(r'^form_register_donatur/$',form_register_donatur,name='form_register_donatur'),
    url(r'^form_register_sponsor/$',form_register_sponsor,name='form_register_sponsor'),
    url(r'^form_register_organisasi/$',form_register_organisasi,name='form_register_organisasi'),
    url(r'^register_organisasi/$',register_organisasi,name='register_organisasi'),
]