from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from sion_login.db_helper import DatabaseConnection

response = {}
def index(request):
	if 'email' in request.session:
		response['login'] = True
		return HttpResponseRedirect(reverse('sion-profile:profile'))

	else:
		response['login'] = False
		return HttpResponseRedirect(reverse('sion-login:index'))

def profile(request):
	if 'email' not in request.session.keys():
		return HttpResponseRedirect(reverse('sion-login:index'))

	conn = DatabaseConnection()
	conn.cursor.execute("select email, nama, alamat_lengkap from sion.user where email='"+request.session['email']+"';")
	row = conn.cursor.fetchone()
	response['email'] = row[0]
	response['nama'] = row[1]
	response['alamat'] = row[2]

	if request.session['role'] == 'donatur':
		conn.cursor.execute("select saldo from sion.donatur where email ='"+request.session['email']+"';")
		row = conn.cursor.fetchone()
		response['saldo'] = row[0]
		html = 'ProfileDonatur.html'

	elif request.session['role'] == 'sponsor':
		conn.cursor.execute("select logo_sponsor from sion.sponsor where email ='"+request.session['email']+"';")
		row = conn.cursor.fetchone()
		response['logo_sponsor'] = row[0]
		html = 'ProfileSponsor.html'

	elif request.session['role'] == 'relawan':
		conn.cursor.execute("select no_hp, tanggal_lahir from sion.relawan where email ='"+request.session['email']+"';")
		row = conn.cursor.fetchone()
		response['no_hp'] = row[0]
		response['tanggal_lahir'] = row[1]

		keahlian = []
		conn.cursor.execute("select keahlian from keahlian_relawan where email = '"+request.session['email']+"';")
		
		for row in conn.cursor.fetchall():
			keahlian.append(row[0])
		response['keahlian_dict'] = keahlian
		
		html = 'ProfileRelawan.html'

	elif request.session['role'] == 'pengurus_organisasi':
		conn.cursor.execute("select organisasi from sion.pengurus_organisasi where email ='"+request.session['email']+"';")
		row = conn.cursor.fetchone()
		response['organisasi'] = row[0]
		html = 'ProfilePengurusOrganisasi.html'

	return render(request, html, response)