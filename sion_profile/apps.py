from django.apps import AppConfig


class SionProfileConfig(AppConfig):
    name = 'sion_profile'
