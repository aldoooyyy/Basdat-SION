"""SION URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import sion_profile.urls as sion_profile
import sion_login.urls as sion_login
import sion_register.urls as sion_register

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='/sion-login/', permanent=True), name='root'),
    url(r'^sion-login/',include(sion_login,namespace='sion-login')),
    url(r'^sion-profile/', include(sion_profile, namespace='sion-profile')),
    url(r'^sion-register/',include(sion_register,namespace='sion-register')),
]